import tensorflow as tf
from tensorflow import keras
from keras import layers
import numpy as np
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

class ConvolutionalNeuralNetwork:
    def __init__(self, x_train, y_train, x_test, y_test, train_batch_size, validation_batch_size):
        self.x_train = x_train
        self.y_train = y_train
        self.x_test = x_test
        self.y_test = y_test
        self.batchSize = 30
        self.validation_batch_size = validation_batch_size
        image_size = x_train.shape[1]
        self.batch_Size = train_batch_size

        #Reshape for inputting into the model
        self.x_train = self.x_train.reshape((60000, 28, 28, 1))
        self.x_test = self.x_test.reshape((10000, 28, 28, 1))
        print(self.x_train.shape)
        print(self.y_train.shape)
        self.y_train = tf.one_hot(self.y_train, 10)
        self.y_test = tf.one_hot(self.y_test, 10)

        self.img_inputs = tf.keras.Input(shape=(image_size, image_size, 1))

        pass

    def displayShapes(self):
        print("x_train shape: " + str(self.x_train.shape))
        print("x_test shape: " + str(self.x_test.shape))
        print("y_train shape: " + str(self.y_train.shape))
        print("y_test shape: " + str(self.y_test.shape))
        pass

    def defineTheModel(self):
        #Convolutional layers first
        conv2dLayer = layers.Conv2D(padding='same', kernel_size=(3, 3), strides=(1, 1), filters=5)(self.img_inputs)
        reluLayer = layers.ReLU()(conv2dLayer)
        maxPoolLayer = layers.MaxPool2D(padding='valid', strides=(2, 2), pool_size=(2, 2))(reluLayer)

        #Fully connected/flattened layers
        layer1 = layers.Flatten()(maxPoolLayer)
        layer2Z = layers.Dense(100)(layer1)
        layer2A = tf.keras.layers.ReLU()(layer2Z)
        layer3 = layers.Dropout(0.2)(layer2A)
        layer4A = layers.Dense(100)(layer3)
        layer4Z = layers.ReLU()(layer4A)
        layer5 = layers.Dropout(0.2)(layer4Z)
        layer6 = layers.Dense(10)(layer5)
        outputLayer = layers.Softmax()(layer6)

        self.model = keras.Model(inputs=self.img_inputs, outputs=outputLayer, name='mnist_model')
        pass

    def displayModelSumary(self):
        self.model.summary()
        pass

    def compileTheModel(self):
        lossFunction = tf.keras.losses.CategoricalCrossentropy(from_logits=False)
        optimizer = tf.keras.optimizers.Adam()
        self.model.compile(optimizer=optimizer, loss=lossFunction, metrics=['accuracy'])
        pass

    def createDatasets(self):
        self.train_dataset = tf.data.Dataset.from_tensor_slices((self.x_train, self.y_train)).batch(batch_size=self.batch_Size)
        self.test_dataset = tf.data.Dataset.from_tensor_slices((self.x_test, self.y_test)).batch(self.validation_batch_size)
        self.train_dataset = self.train_dataset.shuffle(30000)
        pass

    def trainTheNetwork(self, numOfIterations):
        history = self.model.fit(self.train_dataset, epochs=numOfIterations, validation_data=self.test_dataset)
        pass

    def makeAPrediction(self, predictionInput):
        predictionOutput = self.model.predict(predictionInput)
        return predictionOutput

