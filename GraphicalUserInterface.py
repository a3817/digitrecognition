import math
import tkinter as tk
from tkinter import *
import Cell
import numpy as np
import tensorflow as tf
import FullyConnectedNeuralNetwork as nn
import ConvolutionalNeuralNetwork as cnn





class GraphicalUserInterface:
    def __init__(self):
        #Neural network stuff:

        # Get all data
        mnist = tf.keras.datasets.mnist
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        x_train, x_test = x_train / 255.0, x_test / 255.0

        # Convert them all to numpy arrays
        x_train = np.array(x_train)
        y_train = np.array(y_train)
        x_test = np.array(x_test)
        y_test = np.array(y_test)

        # Purely for testing only 0s and 1s
        x_train = np.where(x_train > 0, 1, 0)
        x_test = np.where(x_test > 0, 1, 0)

        #COMMENTING OUT THE FULL CONNECTED NETWORK
        # Train, validate model
        #self.myModel = nn.FullyConnectedNeuralNetwork(x_train, y_train, x_test, y_test)
        #self.myModel.trainTheNetWork(5) #set number of epochs here
        #self.myModel.validateTheNetwork(x_test, y_test)
        self.myModel = cnn.ConvolutionalNeuralNetwork(x_train, y_train, x_test, y_test, 30, 10)
        self.myModel.defineTheModel()
        self.myModel.compileTheModel()
        self.myModel.createDatasets()
        self.myModel.displayModelSumary()
        self.myModel.trainTheNetwork(numOfIterations=10)




        self.neuralNetworkPredictionOutput = [[0,0,0,0,0,0,0,0,0,0]]

        #Construct
        self.window = Tk()
        self.window.resizable(0,0)
        self.window.title("Digit recognition with TF")
        #Set dimensions
        self.setWindowDimensions()

        #Insert the canvas
        self.insertCanvasIntoWindow()

        #Create the cells
        self.xNumber = 560/20
        self.xNumber = math.floor(self.xNumber)
        self.yNumber = 560/20
        self.yNumber = math.floor(self.yNumber)

        self.graph = [[Cell.Cell(j, i, self.myCanvas, 20) for j in range(self.yNumber)] for i in range(self.xNumber)]

        #Draw cells
        self.drawGraph()

        #Add the predict
        self.addPredictButton()

        #Add the labels
        self.addPercentageLabels()

        #Add conclusion label
        self.addConclusionLabel()

        #Add clear canvas button
        self.addClearCanvasButton()

        #Keep the window open
        self.window.mainloop()
        pass

    def setWindowDimensions(self):
        self.window.geometry("1000x600")
        pass

    def insertCanvasIntoWindow(self):
        self.myCanvas = tk.Canvas(self.window, bg="#AB9F43", height=560, width=560)
        self.myCanvas.place(
            x=20,
            y=20
        )
        self.myCanvas.bind("<B1-Motion>", self.callback)

        pass

    def addPredictButton(self):
        self.predictButton = tk.Button(self.window, text="Predict!", command=self.getPrediction)
        self.predictButton.place(x=600, y=20)
        pass

    def addClearCanvasButton(self):
        self.clearCanvasButton = tk.Button(self.window, text="Clear Canvas", command=self.clearCommand)
        self.clearCanvasButton.place(x=600, y=50)
        pass

    def addPercentageLabels(self):

        self.zeroPercentageLabel = tk.Label(self.window, text="0: ")
        self.zeroPercentageLabel.place(x=600, y=80)

        self.onePercentagelabel = tk.Label(self.window, text="1: ")
        self.onePercentagelabel.place(x=600, y=110)

        self.twoPercentageLabel = tk.Label(self.window, text="2: ")
        self.twoPercentageLabel.place(x=600, y=140)

        self.threePercentageLabel = tk.Label(self.window, text="3: ")
        self.threePercentageLabel.place(x=600, y=170)

        self.fourPercentageLabel = tk.Label(self.window, text="4: ")
        self.fourPercentageLabel.place(x=600, y=200)

        self.fivePercentagelabel = tk.Label(self.window, text="5: ")
        self.fivePercentagelabel.place(x=600, y=230)

        self.sixPercentageLabel = tk.Label(self.window, text="6: ")
        self.sixPercentageLabel.place(x=600, y=260)

        self.sevenPercentagelabel = tk.Label(self.window, text= "7: ")
        self.sevenPercentagelabel.place(x=600, y= 290)

        self.eightPercentagelabel = tk.Label(self.window, text="8: ")
        self.eightPercentagelabel.place(x=600, y=320)

        self.ninePercentagelabel = tk.Label(self.window, text="9: ")
        self.ninePercentagelabel.place(x=600, y = 350)
        pass

    def updateLabels(self):

        self.zeroPercentageLabel.config(text="0: " + str(self.neuralNetworkPredictionOutput[0][0]))

        self.onePercentagelabel.config(text="1: " + str(self.neuralNetworkPredictionOutput[0][1]))

        self.twoPercentageLabel.config(text="2: " + str(self.neuralNetworkPredictionOutput[0][2]))

        self.threePercentageLabel.config(text="3: " + str(self.neuralNetworkPredictionOutput[0][3]))

        self.fourPercentageLabel.config(text="4: " + str(self.neuralNetworkPredictionOutput[0][4]))

        self.fivePercentagelabel.config(text="5: " + str(self.neuralNetworkPredictionOutput[0][5]))

        self.sixPercentageLabel.config(text="6: " + str(self.neuralNetworkPredictionOutput[0][6]))

        self.sevenPercentagelabel.config(text="7: " + str(self.neuralNetworkPredictionOutput[0][7]))

        self.eightPercentagelabel.config(text="8: " + str(self.neuralNetworkPredictionOutput[0][8]))

        self.ninePercentagelabel.config(text="9: " + str(self.neuralNetworkPredictionOutput[0][9]))
        pass

    def clearCommand(self):
        # deactivate all cells
        for i in range(self.graph[0].__len__()):
            for j in range(self.graph[0].__len__()):
                self.graph[i][j].deActivateCell()
        # delete all from canvas
        self.myCanvas.delete("all")
        # draw graph
        self.drawGraph()
        pass

    def getPrediction(self):
        #COMMENTING OUT FULLY CONNECTED NETWORK STUFF
        #graphState = self.getStateOfGraph().reshape(1, 28, 28)
        #self.neuralNetworkPredictionOutput = self.myModel.predictClass(graphState)
        #neuralnetworkPredictionConclusion = np.where(self.neuralNetworkPredictionOutput == np.amax(self.neuralNetworkPredictionOutput))[1][0]

        #Set labels with individual prediction %s
        #self.updateLabels()
        #self.updateConclusionLabel(neuralnetworkPredictionConclusion)

        graphState = self.getStateOfGraph().reshape(1, 28, 28)
        self.neuralNetworkPredictionOutput = self.myModel.makeAPrediction(predictionInput=graphState)

        neuralnetworkPredictionConclusion = np.argmax(self.neuralNetworkPredictionOutput)

        self.updateLabels()
        self.updateConclusionLabel(neuralnetworkPredictionConclusion)


        pass

    def drawGraph(self):
        for i in range(self.xNumber):
            for j in range(self.yNumber):
                self.graph[i][j].drawCellInCanvas()
                pass
        pass

    def getStateOfGraph(self):
        stateOfGraph = np.zeros((self.graph[0].__len__(), self.graph[0].__len__()))
        for i in range(self.graph[0].__len__()):
            for j in range(self.graph[0].__len__()):
                if self.graph[i][j].isActive:
                    stateOfGraph[(i,j)] = 1
        #print(stateOfGraph)
        return stateOfGraph

    def callback(self, event):
        #print("clicked at " + str(event.x) + ", " + str(event.y))
        xCoord = math.floor(event.x/20)
        yCoord = math.floor(event.y/20)
        #Loop through the graph and activate the cells around the clicked area
        self.graph[yCoord][xCoord].activateCell()
        self.graph[yCoord+1][xCoord].activateCell()
        self.graph[yCoord-1][xCoord].activateCell()
        self.graph[yCoord][xCoord-1].activateCell()
        self.graph[yCoord][xCoord+1].activateCell()
        self.graph[yCoord+1][xCoord+1].activateCell()
        self.graph[yCoord-1][xCoord-1].activateCell()
        self.graph[yCoord-1][xCoord+1].activateCell()
        self.graph[yCoord+1][xCoord-1].activateCell()
        #After the cells have been activated, then redraw the graph

        self.myCanvas.delete("all")
        self.drawGraph()
        #print(self.getStateOfGraph())
        pass

    def addConclusionLabel(self):
        self.conclusionLabel = tk.Label(self.window)
        self.conclusionLabel.place(x=600, y= 380)
        pass

    def updateConclusionLabel(self, value):
        self.conclusionLabel.config(
            text=str(value),
            font=("Arial",60)
        )

        pass