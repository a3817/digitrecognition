import numpy as np
import tensorflow as tf

class FullyConnectedNeuralNetwork:
    def __init__(self, x_train, y_train, x_test, y_test):
        self.x_train = x_train
        self.y_train = y_train
        self.x_test = x_test
        self.y_test = y_test
        self.batchSize = 30


        self.model = tf.keras.models.Sequential([
            tf.keras.layers.Flatten(input_shape = (28,28)),
            tf.keras.layers.Dense(500, activation='relu'),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Dense(500, activation='relu'),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Dense(10, activation='softmax')
        ])
        self.model.summary()
        pass

    def trainTheNetWork(self, numOfIterations):
        #predictions = self.model(self.x_train[:1]).numpy()
        #tf.nn.softmax(predictions).numpy()

        loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False)
        #loss_fn(self.y_train[:1], predictions).numpy()
        optimizerAdam = tf.keras.optimizers.Adam()

        self.model.compile(optimizer=optimizerAdam, loss=loss_fn, metrics=['accuracy'])
        #print(self.y_train)
        self.model.fit(self.x_train, self.y_train, epochs=numOfIterations, batch_size=self.batchSize)



        pass

    def validateTheNetwork(self, x_test, y_test):
        self.model.evaluate(x_test, y_test, verbose=2)
        pass

    def predictClass(self, predictionInput):
        predictionOutput = self.model.predict(predictionInput)
        #print(predictionOutput)
        #return np.where(predictionOutput == np.amax(predictionOutput))[1][0]
        return predictionOutput




