class Cell:
    def __init__(self, xCoordinate, yCoordinate, canvas, cellLength):
        self.xCoordinate = xCoordinate
        self.yCoordinate = yCoordinate
        self.canvas = canvas
        self.cellLength = cellLength
        self.isActive = False
        pass

    def drawCellInCanvas(self):
        if not self.isActive: #If inactive, outline only
            self.canvas.create_rectangle(
                self.xCoordinate * self.cellLength,
                self.yCoordinate * self.cellLength,
                (self.xCoordinate + 1) * self.cellLength,
                (self.yCoordinate + 1) * self.cellLength,
                outline="black"
            )
        else:               #If active, fill the rectangle
            self.canvas.create_rectangle(
                self.xCoordinate * self.cellLength,
                self.yCoordinate * self.cellLength,
                (self.xCoordinate + 1) * self.cellLength,
                (self.yCoordinate + 1) * self.cellLength,
                fill="black"
            )
        pass
        pass

    def activateCell(self):
        self.isActive = True
        pass

    def deActivateCell(self):
        self.isActive = False
        pass
